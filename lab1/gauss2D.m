function [ kernel ] = gauss2D( sigma )
size = ceil(sigma * 3);
center = ceil(size/2);
kernel = zeros(size);

const = 1/(2*pi*sigma^2);

for i = 1 : size
    for j = 1: size
        kernel(i,j) = const*exp( ((i-center)^2 + (j-center)^2)/ (-2*sigma^2));
    end
end

kernel = kernel./sum(sum(kernel));
end

