function [ kernel ] = gauss1DX( sigma )
size = ceil(sigma * 3);
center = ceil(size/2);
kernel = zeros(0, size);

const = 1/(2*pi*sigma^2);

for i = 1 : size
        kernel(1,i) = const*exp( ((i-center)^2)/ (-2*sigma^2));
end

kernel = kernel./sum(kernel);
end

