clear;
path = 'lena.gif';
I =  im2double(imread(path));
H = ones(3)./9;

tic
I2 = convolution(I,H, 'corresp');
toc
figure;
imshow(I2)
title('Mean filter');