function r = convolution2(img, mask, borderType)
%convolution
i = double(img);
[h,w] = size(i);
[m,n] = size(mask);

if borderType == 0 % mirror
    i = [flipud(i(1:floor(m/2),:)); i; flipud(i(end-floor(m/2)+1:end,:))];
    i = [fliplr(i(:,1:floor(n/2))) i fliplr(i(:,end-floor(n/2)+1:end))];
else % 1 padding
    i = [repmat(i(1,:),floor(m/2),1); i; repmat(i(end,:),2,1)];
    i = [repmat(i(:,1),1,floor(n/2)) i repmat(i(:,end),1,floor(n/2))];

r = zeros(h,w);
for j = 1:h
    for k = 1:w
        r(j,k) = sum(sum(i(j:j+m-1, k:k+n-1).*mask));
    end
end

end
