clear;
path = 'lena.gif';
I =  im2double(imread(path));
[M,N] = size(I);

Dx = [-1 0 1; -1 0 1; -1 0 1];
Dy = [-1 -1 -1; 0 0 0; 1 1 1];

Ix = convolution(I,Dx, 'corresp');
Iy = convolution(I,Dy, 'corresp');
%% B
magnitude = (Ix.^2 + Iy.^2).^0.5;

orientation = atan(Iy./Ix);

figure;
subplot(2,2,1)
imshow(magnitude);
subplot(2,2,2)
imshow(orientation);

%% C
Dx = convolution( gauss1DX(3), Dx, 'corresp');
Dy = convolution(gauss1DX(3)', Dy,  'corresp');

Ix = convolution(I,Dx, 'corresp');
Iy = convolution(I,Dy, 'corresp');

magnitude = (Ix.^2 + Iy.^2).^0.5;

orientation = atan(Iy./Ix);

subplot(2,2,3)
imshow(magnitude);
subplot(2,2,4)
imshow(orientation);
