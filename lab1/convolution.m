function [ I2 ] = convolution( I, H, boarderType )
[R,C] = size(H);
[M,N] = size(I);
I2 = zeros(M,N);


marginR = (R-1)/2;
marginC = (C-1)/2;

paddedImage = zeros(M+marginR*2,N+marginC*2);
paddedImage(marginR+1:M+marginR,marginC+1:N+marginC) = I;

if strcmp(boarderType,'corresp')
    %horizontal padding
    for i = 1:marginR
        paddedImage(i:i,marginC+1:N+marginC) = paddedImage(marginR+1,marginC+1:N+marginC);
        paddedImage(M+2*marginR-i+1:M+2*marginR-i+1,marginC+1:N+marginC) = paddedImage(marginR+M,marginC+1:N+marginC);
    end
    %vertical padding
    for i = 1:marginC
        paddedImage(1:M+2*marginR,i:i) =paddedImage(1:M+2*marginR,marginC+1);
        paddedImage(1:M+2*marginR,N+2*marginC-i+1:N+2*marginC-i+1) =paddedImage(1:M+2*marginR,marginC+N);
    end
    
elseif strcmp(boarderType,'mirror')
    %horizontal padding
    paddedImage(1:marginR,marginC:N+marginC) = flipud(paddedImage(marginR+1:2*marginR,marginC:N+marginC));
    paddedImage(M+marginR:M+2*marginR,marginC:N+marginC) = flipud(paddedImage(M:marginR+M,marginC:N+marginC));
    %vertical padding
    paddedImage(1:M+2*marginR,1:marginC) = fliplr(paddedImage(1:M+2*marginR, marginC+1:2*marginC));
    paddedImage(1:M+2*marginR,N+marginC+1:N+2*marginC) = fliplr(paddedImage(1:M+2*marginR,N+1:marginC+N));
    
end

for m = marginR + 1:M + marginR
    for n = marginC+1:N +marginC
        I2(m - marginR,n -marginC)=sum(sum(paddedImage(m-marginR:m+marginR, n-marginC: n+marginC).*H));
    end
end


end

