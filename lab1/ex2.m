
path = 'lena.gif';
I =  im2double(imread(path));

for sigma = 1:2:3
H = gauss2D(sigma);
tic
I2 = convolution(I,H, 'corresp');
toc

figure
subplot(1,2,1)
hold on;
title('Gauss 2D')
imshow(I2)
%%
Hx = gauss1DX(sigma);
Hy = Hx';

tic
Ix = convolution(I,Hx, 'corresp');
I3 = convolution(Ix, Hy, 'corresp');
toc
subplot(1,2,2)
hold on;
title('Gauss 1D x2')
imshow(I3);

tic
conv2(I,H);
toc
tic
conv2(Hx,Hy,I);
toc

end