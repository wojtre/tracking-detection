function h = DLT(ref, warped)
%ref, warped = 2xN/3xN
% Convert to homogeneous coordinates if 2xN
if (size(p1,1) ~= 3)
    ref = padarray(ref,[1 0],1,'post');
    warped = padarray(warped,[1 0],1,'post');
end

% Normalization
[ref,t1] = normalize2d(p1);
[warped,t2] = normalize2d(p2);

x2 = normalize2d(1,:);
y2 = normalize2d(2,:);
w2 = normalize2d(3,:);

% Ah = 0
a = [];
for i=1:size(p1,2)
    a = [a; zeros(3,1)'     -w2(i)*ref(:,i)'   y2(i)*ref(:,i)'; ...
            w2(i)*ref(:,i)'   zeros(3,1)'     -x2(i)*ref(:,i)'];
end

% Obtain the SVD of A. The unit singular vector corresponding to the
% smallest singular value is the solucion h. A = UDV' with D diagonal with
% positive entries, arranged in descending order down the diagonal, then h
% is the last column of V.
[u,d,v] = svd(a);

h = reshape(v(:,9),3,3)';

% Desnormalization
h = inv(t2) * h * t1;
    


    