function [nodes leafs] = loadTree(filename)
f = fopen(filename, 'r');
line = fgetl(f);
num_nodes = str2double(line);
formatSpec = '%d %d %d %f %d %d %d %d %d %d %d';
sizeA = [11 num_nodes];
nodes = fscanf(f, formatSpec, sizeA);
nodes = nodes';

line = fgetl(f);
line = fgetl(f);
num_leafs = str2double(line);
formatSpec = '%d %f %f';
sizeA = [3 num_leafs];
leafs = fscanf(f, formatSpec, sizeA);
leafs = leafs';

fclose(f);
end