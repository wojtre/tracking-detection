function [ heatMap, ymax, xmax, pixelVotes ] = calculateHeatMap( decisions )
[rows, cols, z] = size(decisions);

heatMap = zeros(rows, cols);
for y = 1 : rows
    for x = 1 : cols
        heatMap(max(1,min(rows,round(decisions(y,x,2)+ y))),max(1,min(cols,round(decisions(y,x,1)+ x)))) = heatMap(max(1,min(rows,round(decisions(y,x,2)+ y))),max(1,min(cols,round(decisions(y,x,1)+ x))))+1;
    end
end

[maxA,ind] = max(heatMap(:));
[ymax,xmax] = ind2sub(size(heatMap),ind);

pixelVotes = zeros(rows,cols);

for y = 1 : rows
    for x = 1 : cols
        if( max(1,min(rows,round(decisions(y,x,2)+ y)))== ymax && max(1,min(cols,round(decisions(y,x,1)+ x)))==xmax)
            pixelVotes(y,x)=1;
        end
    end
end

end

