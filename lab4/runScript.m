img = imread('2007_000032.jpg');
integral = integralImages(img);
[rows, cols, z] = size(integral);

treesNumber = 10;
decisions = zeros(rows,cols,2);
tic


for n = 0 : treesNumber - 1
    fileName=sprintf('Tree%d.txt', n);
    [nodes leaves]=loadTree(fileName);
  
    for y = 1 : rows;
        for x = 1 : cols;
            %% compute decision
            
            [px,py]=computeDecision(integral,x,y,nodes,leaves);
            
            %% compute decision
            
            px=px/treesNumber;
            py=py/treesNumber;
            decisions(y,x,1) = px + decisions(y,x,1);
            decisions(y,x,2) = py + decisions(y,x,2);
            
        end
    end
end
toc


figure;
[heat,y,x, pixelVotes]=calculateHeatMap(decisions);
imshow(mat2gray(heat, [0,20]))
 rectangle('Position',[x y 5 5], 'edgecolor','green')
 figure;
 imshow(mat2gray(pixelVotes));
