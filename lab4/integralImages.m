function [integralRGB] = integralImages(img)
%img = double(img);
[h w num_channels] = size(img);
red = img(:,:,1);
green = img(:,:,2);
blue = img(:,:,3);
integralR = integralImage(red);
integralG = integralImage(green);
integralB = integralImage(blue);
integralRGB = zeros(h+1, w+1, num_channels);
integralRGB(:,:,1) = integralB;
integralRGB(:,:,2) = integralG;
integralRGB(:,:,3) = integralR;
integralRGB(1,:,:) = [];
integralRGB(:,1,:) = [];
end