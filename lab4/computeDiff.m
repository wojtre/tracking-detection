function [ result ] = computeDiff( integralImg, x, y, x0, y0, z0, x1, y1, z1, s)

[rows, cols, z] = size(integralImg);
z0 = z0 + 1;
z0 = min(z0,3);
average0 = integralImg(max(1,min(rows, y+y0+s)),max(1,min(cols, x+x0+s)), z0) - integralImg( max(1,min(rows, y+y0+s)),min(cols,max(1, x+x0-s)), z0) - integralImg( min(rows,max(1, y+y0-s)),max(1,min(cols, x+x0+s)), z0) + integralImg( min(rows,max(1, y+y0-s)),min(cols,max(1, x+x0-s)), z0);
average0 = average0 / (2*s+1)^2;
z1 = z1 + 1;
z1 = min(z1,3);
average1 = integralImg(max(1,min(rows, y+y1+s)),max(1,min(cols, x+x1+s)), z1) - integralImg( max(1,min(rows, y+y1+s)),min(cols,max(1, x+x1-s)), z1) - integralImg( min(rows,max(1, y+y1-s)),max(1,min(cols, x+x1+s)), z1) + integralImg( min(rows,max(1, y+y1-s)),min(cols,max(1, x+x1-s)), z1);
average1 = average1 / (2*s+1)^2;
result = average0 - average1;

end

