function [px, py ] = computeDecision( img, x, y, nodes, leaves )
cl=2;
cr=3;
t=4;
x0=5;
y0=6;
z0=7;
x1=8;
y1=9;
z1=10;
s=11;

node = 0;
values = nodes(node+1,:);
% diff =  computeBoxAverage(img,x,y,values(x0),values(y0),values(z0)+ 1, values(s)) - computeBoxAverage(img,x,y, values(x1),values(y1),values(z1)+1,values(s));
diff = computeDiff(img,x,y,values(x0),values(y0),values(z0),values(x1),values(y1),values(z1),values(s));
if (diff < nodes(node+1,t))
    node = nodes(node+1,cl);
else
    node = nodes(node+1,cr);
end

while (node > 0.5)
values = nodes(node+1,:);

%  diff =  computeBoxAverage(img,x,y,values(x0),values(y0),values(z0)+ 1, values(s)) - computeBoxAverage(img,x,y, values(x1),values(y1),values(z1)+1,values(s));

 diff = computeDiff(img,x,y,values(x0),values(y0),values(z0),values(x1),values(y1),values(z1),values(s)); 
 if (diff < nodes(node+1,t))
    node = nodes(node+1,cl);
else
    node = nodes(node+1,cr);
end   
    
end

node = abs(node);

px = leaves(node+1, 2);
py = leaves(node+1,3);


end

