function [ average ] = computeBoxAverage( img, x, y, x0, y0, z0, s )
[rows, cols, z] = size(img);
z0 = min(z0,3);
average = img(max(1,min(rows, y+y0+s)),max(1,min(cols, x+x0+s)), z0) - img( max(1,min(rows, y+y0+s)),min(cols,max(1, x+x0-s)), z0) - img( min(rows,max(1, y+y0-s)),max(1,min(cols, x+x0+s)), z0) + img( min(rows,max(1, y+y0-s)),min(cols,max(1, x+x0-s)), z0);
average = average / (2*s+1)^2;
end

