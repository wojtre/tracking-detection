function r = multiscaleHarris(img, sigma, n, s0, k, alpha, t)

img = double(img);
[h,w] = size(img);
kernel = [ones(sigma*3, floor(sigma*3/2))*-1, zeros(sigma*3, 1), ones(sigma*3, ceil(sigma*3/2))];
Hx = kernel .* gauss2D(sigma);
Hy = Hx';
Ix = conv2(img, Hx, 'same');
Iy = conv2(img, Hy, 'same');
r = zeros(h,w);
for i = 1 : w
    for j = 1 : h
        M = [Ix(j,i)^2 Ix(j,i)*Iy(j,i); Ix(j,i)*Iy(j,i) Ix(j,i)^2];
        R = det(M) - alpha * trace(M)^2;
        if R > t
            r(j,i) = R;
        end
    end
end

end
            
            
    
            
        
        
