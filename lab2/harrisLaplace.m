function [imgInterestPoints] = harrisLaplace(img, resLevels)
%sigma_0 = 1;
%sigma_k = 1.5; 
%sigma_n = 17.5;
t_l = 0.2;
[h,w] = size(img);

% maybe iterate over scale in here instead of in multiscale harris
%F(x, sn) with sn = k^n*s0; s = sn
imgScaleSpace = multiscaleHarris2(img, resLevels, 1, 1.2, 0.06, 0);
interestPoints = zeros(resLevels, h, w);
interestPoints(1,:,:) = imgScaleSpace(1,:,:) > max(imgScaleSpace(2,:,:), t_l);
for i = 2:(resLevels-1)
    interestPoints(i,:,:) = imgScaleSpace(i,:,:) > max(max(imgScaleSpace(i-1,:,:),imgScaleSpace(i+1,:,:)), t_l);
end
interestPoints(resLevels,:,:) = imgScaleSpace(resLevels,:,:) > max(imgScaleSpace(resLevels-1,:,:),t_l);

imgInterestPoints = img;
shapeInserter = vision.ShapeInserter('Shape','Circles');

for i = 1:resLevels
    for j = 1:h
        for k = 1:w
            if interestPoints(i,j,k) > 0
                circle = int([j k 3*i]);
                imgInterestPoints = shapeInserter(imgInterestPoints, circle);
            end
        end
    end
end

end
       
    
    