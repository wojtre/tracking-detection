function [ imageScaleSpace ] = multiscaleHarris2( img,  n, s0, k, alpha, t )
img = double(img);
[h,w] = size(img);
imageScaleSpace= zeros(n,h,w);
%gaussian derivative and integration scale
sigmaD = 1;
for i = 1 : n
gd = fspecial('gaussian',3,sigmaD);
gi = fspecial('gaussian',max(1,fix(6*s0+1)),s0)

% derivatives kernels
Dx = [-1 0 1; -1 0 1; -1 0 1];
Dx = conv2(Dx,gd, 'same');
Dy = Dx';
% derivatives
Ix = sigmaD*conv2(img, Dx, 'same');
Iy = sigmaD*conv2(img, Dy, 'same');
Ix2 = Ix.^2;
Iy2 = Iy.^2;
Ixy = Ix.*Iy;
%apply integration scale


GIx2=conv2(Ix2, gi, 'same');
GIy2=conv2(Iy2, gi, 'same');
GIxy=conv2(Ixy, gi, 'same');
R = (GIx2.*GIy2 - GIxy.^2) - alpha*(GIx2 + GIy2).^2;
R(R<t) = 0;
%% non maximal suppression
Dx = [-1 0 1; -1 0 1; -1 0 1];
Dy = Dx';
RIx = conv2(R, Dx, 'same');
RIy = conv2(R, Dy, 'same');

psi = atan(RIy./RIx);
%orthogonal
psi = mod(psi+pi/2,pi);

mask = zeros(size(R));
[ix,iy] = meshgrid(1:w,1:h);
%Pixel masks for directions:
mask1 = (psi>=0 & psi<pi/8) | (psi>=7*pi/8 & psi<pi);
mask2 = ( psi>=pi/8 & psi<3*pi/8);
mask3 = ( psi>=3*pi/8 & psi<5*pi/8);
mask4 = ( psi>=5*pi/8 & psi<7*pi/8); 

%case 1: x-direction
idx = find(mask1 & ix>1 & iy>1 & ix<w & iy<h)
idx1 = idx + h;
idx2 = idx - h;
mask(idx(find(R(idx) > max(R(idx1),R(idx2))))) = 1;
%case 2: /-direction
idx = find(mask2 & ix>1 & iy>1 & ix<w & iy<h)
idx1 = idx + h - 1;
idx2 = idx - h + 1;
mask(idx(find(R(idx) > max(R(idx1),R(idx2))))) = 1;
%case 3: y-direction
idx = find(mask3 & ix>1 & iy>1 & ix<w & iy<h)
idx1 = idx + 1;
idx2 = idx - 1;
mask(idx(find(R(idx) > max(R(idx1),R(idx2))))) = 1;
%case 4: \-direction
idx = find(mask4 & ix>1 & iy>1 & ix<w & iy<h)
idx1 = idx + h + 1;
idx2 = idx - h -1;
mask(idx(find(R(idx) > max(R(idx1),R(idx2))))) = 1;

%apply mask
R = R.*mask;

imageScaleSpace(i,:,:) = R;
%figure;
%imshow(reshape(imageScaleSpace(i,:,:),h,w));
s0 = s0*k;
sigmaD = sigmaD*k;
end

end

