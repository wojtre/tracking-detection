% run('/home/wojtek/Documents/vlfeat-0.9.20/toolbox/vl_setup.m')
% run('/home/wojtek/Documents/matconvnet-1.0-beta23/matlab/vl_setupnn.m')

clear all;
close all;
im1 = imresize(imread('shell.jpg'), 0.3 );
im2 = imresize(imread('test_shell1.jpg'), 0.5) ;
Ia = im2single(rgb2gray(im1)) ;
Ib = im2single(rgb2gray(im2)) ;
%%
cellSize = 8 ;
hog1 = vl_hog(Ia, cellSize, 'verbose') ;
imhog1 = vl_hog('render', hog1, 'verbose') ;
clf ; imagesc(imhog1) ; colormap gray ;

figure;
hog2 = vl_hog(Ib, cellSize) ;
imhog2 = vl_hog('render', hog2) ;
clf ; imagesc(imhog2) ; colormap gray ;


minScale = -1 ;
maxScale = 2 ;
numOctaveSubdivisions = 3 ;
scales = 2.^linspace(...
  minScale,...
  maxScale,...
  numOctaveSubdivisions*(maxScale-minScale+1)) ;

max_global = 0;
max_img = im1;
objWidth = size(hog1,2);
objHeight = size(hog1,1);


for s = scales
     Iresize = imresize(Ib,1/s);
    hog2 = vl_hog(Iresize, cellSize) ;
    scores = vl_nnconv(hog2, hog1, []);
    [max_num,max_idx] = max(scores(:));
    max_num
    s
    if max_global <max_num
         max_global = max_num;
         [Ymax, Xmax]=ind2sub(size(scores),find(scores==max_num));
        
         x = (Xmax - 1) * cellSize * s + 1 ;
         y = (Ymax - 1) * cellSize * s + 1 ;
         detectedRectangle = [
         x - cellSize*objWidth*s;
         y - cellSize*objHeight*s;
          cellSize*objWidth*s*2 ;
          cellSize*objHeight*s*2 ;] ;  
    end
  
end
figure;
imagesc(im2);
rectangle('Position', detectedRectangle, 'edgecolor','green')




