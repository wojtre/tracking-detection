%run('C:\Users\Wojtek\Documents\vlfeat-0.9.20\toolbox\vl_setup.m')

clear all;
close all;
im1 = imread('shell.jpg') ;
im2 = imread('test_shell1.jpg') ;
image(im1) ;
Ia = single(rgb2gray(im1)) ;
%% a)
[fa,da] = vl_sift(Ia) ;
perm = randperm(size(fa,2)) ;
sel = perm(1:50) ;
h1 = vl_plotframe(fa(:,sel)) ;
h2 = vl_plotframe(fa(:,sel)) ;
set(h1,'color','k','linewidth',3) ;
set(h2,'color','y','linewidth',2) ;
h3 = vl_plotsiftdescriptor(da(:,sel),fa(:,sel)) ;
set(h3,'color','g') ;

figure;

image(im2) ;
Ib = single(rgb2gray(im2)) ;
[fb,db] = vl_sift(Ib) ;
perm = randperm(size(fb,2)) ;
sel = perm(1:50) ;
h1 = vl_plotframe(fb(:,sel)) ;
h2 = vl_plotframe(fb(:,sel)) ;
set(h1,'color','k','linewidth',3) ;
set(h2,'color','y','linewidth',2) ;
h3 = vl_plotsiftdescriptor(db(:,sel),fb(:,sel)) ;
set(h3,'color','g') ;

%% b)
[matches, scores] = vl_ubcmatch(da, db) ;
beginPoints = [fa(1,matches(1,:));fa(2,matches(1,:))]';
endPoints = [fb(1,matches(2,:));fb(2,matches(2,:))]';
dh1 = max(size(im2,1)-size(im1,1),0) ;
dh2 = max(size(im1,1)-size(im2,1),0) ;

figure ; clf ;
imagesc([padarray(im1,dh1,'post') padarray(im2,dh2,'post')]) ;
o = size(im1,2) ;
line([fa(1,matches(1,:));fb(1,matches(2,:))+o], ...
     [fa(2,matches(1,:));fb(2,matches(2,:))]) ;
axis image off ;
drawnow ;

figure;
showMatchedFeatures(im1,im2,...
  beginPoints, endPoints );
%% c)
[tform,inlierPtsOut,inlierPtsIn] = estimateGeometricTransform(...
         beginPoints,endPoints,'affine');

 figure ; clf ;
imagesc([padarray(im1,dh1,'post') padarray(im2,dh2,'post')]) ;
o = size(im1,2) ;
 line([inlierPtsOut(:,1)';inlierPtsIn(:,1)'+o], ...
     [inlierPtsOut(:,2)';inlierPtsIn(:,2)']) ;
 rectangle('Position',[min(inlierPtsIn(:,1)+o), min(inlierPtsIn(:,2)), max(inlierPtsIn(:,1)) - min(inlierPtsIn(:,1)), max(inlierPtsIn(:,2)) - min(inlierPtsIn(:,2))], 'edgecolor','green')

figure;
showMatchedFeatures(im1,im2,...
    inlierPtsOut,inlierPtsIn);

