function [ energy ] = energy(RT,A, M,m )
projection=A*(eul2rotm(RT(:,1)')*M +RT(:,2) );
projection=projection./projection(3,:);
energy=sum(sum((projection-m).^2));
end

