
run('C:\Users\Wojtek\Documents\vlfeat-0.9.20\toolbox\vl_setup.m')
run('C:\Users\Wojtek\Documents\vlfeat-0.9.20\toolbox\mex\mexw64\vl_version.mexw64')
numfiles = 44;
close all;


%% EX1
R0 = eye(3,3);
R0eul = rotm2eul(R0)';
T0 = zeros(3,1);
A = [472.3 0.64 329; 0 471 268.3; 0 0 1];
Ainv = A^-1;
R_all(1,:,:)=R0;
T_all(1,:)= T0;

im0 = imread('img_sequence\0000.png') ;
I0 = single(rgb2gray(im0));
[f0,d0] = vl_sift(I0);


%% EX2
for i = 1 : numfiles
    %% names
    if i < 10
        myfilename = sprintf('img_sequence\\000%d.png', i);
    else
        myfilename = sprintf('img_sequence\\00%d.png', i);
    end
    
    %% names
    im1 = imread(myfilename) ;
    I1 = single(rgb2gray(im1));
    [f1,d1] = vl_sift(I1);
    
    [matches, scores] = vl_ubcmatch(d0, d1) ;
    beginPoints = [f0(1,matches(1,:));f0(2,matches(1,:))];
    endPoints = [f1(1,matches(2,:));f1(2,matches(2,:))];
    
    [H corrPtIdx]=findHomography(beginPoints,endPoints);
    
    beginPointsCorrect = beginPoints(:,corrPtIdx);
    endPointsCorrect = endPoints(:,corrPtIdx);
    
    %     showMatchedFeatures(im0,im1,...
    %         beginPointsCorrect', endPointsCorrect' );
    
        figure ; clf ;
        dh0 = max(size(im1,1)-size(im0,1),0) ;
        dh1 = max(size(im0,1)-size(im1,1),0) ;
        imagesc([padarray(im0,dh0,'post') padarray(im1,dh1,'post')]) ;
        o = size(im0,2) ;
    
        line([beginPointsCorrect(1,:);endPointsCorrect(1,:)+o], ...
            [beginPointsCorrect(2,:);endPointsCorrect(2,:)]) ;
        axis image off ;
        drawnow ;
    
    %% EX3
    m1 = beginPointsCorrect';
    m0 = endPointsCorrect';
    [r,c] = size(m1);
    m=[m1 ones(r,1)]';
    m0=[m0 ones(r,1)]';
    M0=Ainv*m0;
    M1=Ainv*m;
    M=M0;
    
    RT = [R0eul T0];
    fun = @(RT)energy(RT,A, M,m );
    [RTnew,fval] = fminsearch(fun,RT);
    R_all(i+1,:,:)=eul2rotm(RTnew(:,1)');
    T_all(i+1,:)= RTnew(:,2);
    
    % copy values
    R0eul = RTnew(:,1);
    T0=RTnew(:,2);
end

figure;
hold on;
grid on;
X = -reshape(R_all(1,:,:),3,3)*T_all(1,:)';
for i=2:numfiles+1
    X2 = -reshape(R_all(i,:,:),3,3)'*T_all(i,:)';
    pts = [X';X2'];
    plot3(pts(:,1), pts(:,2), pts(:,3))
    text( X(1),X(2), X(3), [num2str(i-1)] );
    X=X2;
end
